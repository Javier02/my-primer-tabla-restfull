const express = require('express');

function plus(req, res, next){
  res.send(req.params.n1 + req.params.n2);
};

function multiply(req, res, next){
  res.send(req.body.n1 + req.body.n2);
};

function divide(req, res, next){
  res.send(req.body.n1 + req.body.n2);
};

function minus(req, res, next){
  res.send(req.params.n1 - req.params.n2);
};

module.exports = {
  plus,
  multiply,
  divide,
  minus
};
