const express = require('express');

function create(req, res, next){
  //express.json();
  //console.log(req.body);
  res.send(`Crea un Usuario con nobre ${req.body.name}`);
};

function list(req, res, next){
  //res.send("Lista los Usuarios.");
  res.send(`Crea un usuario con nombre ${req.body.name}`);
};

function index(req, res, next){
  res.send(`Lista un Usuario con un ${req.params.id} Id especifico.`);
};

function update(req, res, next){
  res.send("Edita un Usuario con un Id especifico.");
};

function destroy(req, res, next){
  res.send("Borra un Usuario con un Id especifico.");
};

module.exports = {
  create,
  list,
  index,
  update,
  destroy
};
